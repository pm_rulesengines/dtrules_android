/** 
 * Copyright 2004-2009 DTRules.com, Inc.
 *   
 * Licensed under the Apache License, Version 2.0 (the "License");  
 * you may not use this file except in compliance with the License.  
 * You may obtain a copy of the License at  
 *   
 *      http://www.apache.org/licenses/LICENSE-2.0  
 *   
 * Unless required by applicable law or agreed to in writing, software  
 * distributed under the License is distributed on an "AS IS" BASIS,  
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  
 * See the License for the specific language governing permissions and  
 * limitations under the License.  
 **/ 
package pl.me.sampledtrules;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import com.dtrules.entity.IREntity;
import com.dtrules.infrastructure.RulesException;
import com.dtrules.interpreter.IRObject;
import com.dtrules.session.IRSession;
import com.dtrules.testsupport.ATestHarness;
import com.dtrules.testsupport.ITestHarness;
import com.dtrules.xmlparser.XMLPrinter;

import java_cup.Main;

public class TestSampleProject2 extends ATestHarness {

	public boolean  Trace()                   { return true;                            }
	public boolean  Console()                 { return false;                            }
    //jak Console() jest na true, to wszystko wypisuje 2 razy
	public String   getPath()                 { return MainActivity.path;      }
	public String   getRulesDirectoryPath()   { return getPath()+"xml/";                }
	public String   getRuleSetName()          { return "DefaultRuleSet";                }
	public String   getDecisionTableName()    { return "Table0_1";           }
	public String   getRulesDirectoryFile()   { return "DTRules.xml";                   }
    //getRulesDirectoryFile()   { return getPath()+"DTRules.xml";  } - nic nie wnioskowal przez to

	public static void main(String[] args) {
		ITestHarness t = new TestSampleProject2();
		MainActivity.addTextToTextView(MainActivity.textView, "Before t.runTests()");
		t.runTests();
        MainActivity.addTextToTextView(MainActivity.textView, "After t.runTests()");
	}

	public void printReport(int runNumber, IRSession session, PrintStream _out) throws RulesException {
		System.out.println("printReport()");
		MainActivity.addTextToTextView(MainActivity.textView,"REPORT:");
//            XMLPrinter xout = new XMLPrinter(_out);
//	        xout.opentag("results","runNumber",runNumber);
	        /*RArray results = session.getState().find("job.results").rArrayValue();
	        for(IRObject r :results){
	            IREntity result = r.rEntityValue();

	            xout.opentag("Client","id",result.get("client_id").stringValue());
	            prt(xout,result,"totalGroupIncome");
	            prt(xout,result,"client_fpl");
	            if(result.get("eligible").booleanValue()){
	                xout.opentag("Approved");
		                prt(xout,result,"program");
		                prt(xout,result,"programLevel");
		                RArray notes = result.get("notes").rArrayValue();
		                xout.opentag("Notes");
		                    for(IRObject n : notes){
		                       xout.printdata("note",n.stringValue());
		                    }
	                    xout.closetag();
	                xout.closetag();
	            }else{
	                xout.opentag("NotApproved");
	                    prt(xout,result,"program");
	                    RArray notes = result.get("notes").rArrayValue();
	                    xout.opentag("Notes");
	                        for(IRObject n : notes){
	                           xout.printdata("note",n.stringValue());
	                        }
	                    xout.closetag();
	                xout.closetag();
	            }
	            xout.closetag();
	        }*/
//	        xout.close();
//		System.out.println("Testy: "+session.getState().find("kandydat.testy").intValue());
//		System.out.println("Rozmowa: "+session.getState().find("kandydat.rozmowa").intValue());
//		System.out.println("Kwalifikacje: " + session.getState().find("kandydat.kwalifikacje").intValue());
//		System.out.println("Wynik: " + session.getState().find("kandydat.wynik").intValue());
//        System.out.println("Ocena: " + session.getState().find("kandydat.ocena").stringValue());
        //System.out.println("Coś czego nie ma w encji:"+session.getState().find("kandydat.cos"));
        // ^to dalo na wyjsciu Coś czego nie ma w encji:null
        //System.out.println("Coś czego nie ma w encji:"+session.getState().find("kandydat.cos").stringValue());
        // ^to rzucilo java.lang.NullPointerException: Attempt to invoke interface method 'java.lang.String com.dtrules.interpreter.IRObject.stringValue()' on a null object reference

//        MainActivity.addTextToTextView(MainActivity.textView, "Testy: " + session.getState().find("kandydat.testy").intValue());
//        MainActivity.addTextToTextView(MainActivity.textView,"Rozmowa: "+session.getState().find("kandydat.rozmowa").intValue());
//        MainActivity.addTextToTextView(MainActivity.textView,"Kwalifikacje: " + session.getState().find("kandydat.kwalifikacje").intValue());
//        MainActivity.addTextToTextView(MainActivity.textView,"Wynik: " + session.getState().find("kandydat.wynik").intValue());
//        MainActivity.addTextToTextView(MainActivity.textView,"Ocena: " + session.getState().find("kandydat.ocena").stringValue());

        try {
            BufferedReader br = new BufferedReader(new java.io.FileReader(getPath()
                    +"testfiles/output/TestCase_001_entities_after.xml"));
            String line;
            while ( (line = br.readLine()) != null) {
                if (!line.contains("<attribute name")) continue;
                if (line.contains("mapping*key")) continue;
				if (line.contains("></attribute>")) continue;
                System.out.println(line.trim());
                MainActivity.addTextToTextView(MainActivity.textView, line.trim());
            }

        } catch (FileNotFoundException e) {
            System.err.println("Error! Cannot open result file!");
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println("Error while reading content of the result file!");
            e.printStackTrace();
        }
	}

//	    private void prt(XMLPrinter xout, IREntity entity, String attrib){
//	        IRObject value = entity.get(attrib);
//	        xout.printdata(attrib,value);
//	    }

}
	    
