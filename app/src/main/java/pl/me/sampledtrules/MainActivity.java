package pl.me.sampledtrules;

import android.Manifest;
import android.app.Activity;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import excel.util.Excel2XML;


public class MainActivity extends Activity {

    static TextView textView;
    private static List<Long> memoryUsage = new LinkedList<>();
    private static List<Float> processorUsage = new LinkedList<>();
    private static boolean ifContinue = true;
    private Date start, end;
    private BufferedWriter bw;
    private static boolean onlyTest;

    static String path    = Environment.getExternalStorageDirectory().getAbsolutePath()+"/sampledtrules/";
    public static String RULE_SET = "DefaultRuleSet";
    public static String RULES_CONFIG = "DTRules.xml";
    private static String REPOSITORY = path+"repository";

    private RadioGroup radioGroup;
    private RadioButton radioButton;
    private String selectedText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        int permissionCheck;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            permissionCheck = this.checkSelfPermission("Manifest.permission.READ_EXTERNAL_STORAGE");
            if (permissionCheck != 0) {
                this.requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1001); //Any number
            }
        }
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        textView = (TextView) findViewById(R.id.tv);
        textView.setMovementMethod(new ScrollingMovementMethod());
        radioGroup = (RadioGroup) findViewById(R.id.grupa);
        File dtRulesBaseDir = new File(path);
        for (File model : dtRulesBaseDir.listFiles()) {
            radioButton = new RadioButton(this);
            radioButton.setText(model.getName());
            radioGroup.addView(radioButton);
        }
//        new Monitor().execute();
    }

    public void compile(View view) {
        configurePaths();
        try {
            textView.setKeepScreenOn(true);
            addTextToTextView(textView, "Compile");
            Excel2XML.compile(path, RULES_CONFIG, RULE_SET, REPOSITORY);
            addTextToTextView(textView, "After compiling");
            textView.setKeepScreenOn(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Excel2XML.compile(PATH, RULES_CONFIG, RULE_SET, REPOSITORY, new String[]{"request", "output"});
    }

    public void test(View view) {
        addTextToTextView(textView,"Test");
        textView.setKeepScreenOn(true);
        onlyTest = true;
        configurePaths();
        try {
            bw = new BufferedWriter(new FileWriter(Environment.getExternalStorageDirectory()
                    .getAbsolutePath()+"/benchmark/dtrules.txt",true));
            start = new Date();
            TestSampleProject2.main(null);
//            end = new Date();
//            ifContinue = false;
            addTextToTextView(textView, "After test");
            radioGroup.setVisibility(View.GONE);
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
            textView.setKeepScreenOn(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void compileAndTest(View view) {
        addTextToTextView(textView, "Before compile&test");
        System.out.println("Before compile&test");
        textView.setKeepScreenOn(true);
        onlyTest = false;
        configurePaths();
        try {
//            bw = new BufferedWriter(new FileWriter(Environment.getExternalStorageDirectory()
//                    .getAbsolutePath()+"/benchmark/dtrules.txt",true));
            start = new Date();
            Excel2XML.compile(path, RULES_CONFIG, RULE_SET, REPOSITORY);
            System.out.println("After compiling");
            TestSampleProject2.main(null);
            System.out.println("After testing");
//            end = new Date();
//            ifContinue = false;
            addTextToTextView(textView, "After compile&test");
            radioGroup.setVisibility(View.GONE);
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
            textView.setKeepScreenOn(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void addTextToTextView(TextView t, String s) {
        t.setText(t.getText().toString() + "\n " + s);
    }

    private float readUsage() {
        try {
            RandomAccessFile reader = new RandomAccessFile("/proc/stat", "r");
            String load = reader.readLine();

            String[] toks = load.split(" +");  // Split on one or more spaces

            long idle1 = Long.parseLong(toks[4]);
            long cpu1 = Long.parseLong(toks[2]) + Long.parseLong(toks[3]) + Long.parseLong(toks[5])
                    + Long.parseLong(toks[6]) + Long.parseLong(toks[7]) + Long.parseLong(toks[8]);

            try {
                Thread.sleep(360);
            } catch (Exception ignored) {}

            reader.seek(0);
            load = reader.readLine();
            reader.close();

            toks = load.split(" +");

            long idle2 = Long.parseLong(toks[4]);
            long cpu2 = Long.parseLong(toks[2]) + Long.parseLong(toks[3]) + Long.parseLong(toks[5])
                    + Long.parseLong(toks[6]) + Long.parseLong(toks[7]) + Long.parseLong(toks[8]);

            return (float)(cpu2 - cpu1) / ((cpu2 + idle2) - (cpu1 + idle1));

        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return 0;
    }

    public class Monitor extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            while (ifContinue) {
                memoryUsage.add(Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory());
//                proc = readUsage();
//                System.out.println(proc);
                processorUsage.add(readUsage());
            }
            System.out.println("MONITOR: KONIEC");
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Long [] mem = memoryUsage.toArray(new Long[memoryUsage.size()]);
            long minMem = mem[0];
            long maxMem = mem[0];
            for (long m : mem) {
                if (m < minMem) minMem = m;
                if (m > maxMem) maxMem = m;
            }

            Float [] proc = processorUsage.toArray(new Float[processorUsage.size()]);
            float minProc = proc[0];
            float maxProc = proc[0];
            for (float p : proc) {
                if (p < minProc && p >= 0) minProc = p;
                if (p > maxProc) maxProc = p;
            }
            try {
                bw.write(printDateTime());
                bw.write("("+selectedText+") ");
                bw.write("Mem diff: ");
                bw.write(String.valueOf( (maxMem - minMem) / 1024L));
                bw.write(" KB , Processor diff: ");
                bw.write(String.valueOf(maxProc-minProc));
                bw.write(", Time diff: ");
                bw.write(String.valueOf(end.getTime() - start.getTime()));
                bw.write(" ms "+(onlyTest ? "(tylko wykonanie)" : "")+"\n-------\n");
                bw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    private String printDateTime() {
        Calendar c = Calendar.getInstance();
        return String.format("%tY-%tm-%td %tH:%tM:%tS : ", c, c, c, c, c, c);
    }

    private void configurePaths() {
        int radioButtonID = radioGroup.getCheckedRadioButtonId();
        View radioButton = radioGroup.findViewById(radioButtonID);
        int idx = radioGroup.indexOfChild(radioButton);
        RadioButton r = (RadioButton)  radioGroup.getChildAt(idx);
        selectedText = r.getText().toString();
        path = path+selectedText+"/";
        REPOSITORY = path+"repository";
    }
}
