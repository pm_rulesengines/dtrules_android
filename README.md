# README #

This README contains specific information for DTRules rules engine.

### Essential files ###

Before launching the app, you need to upload a directory with a model to the phone/tablet memory. By default, the model directory has to be placed in the internal storage (not on the memory card) and has to be saved inside "sampledtrules" directory. A directory with a sample model is available in "Downloads" directory.